## minsleep - determine minimum sleep time for a system
minsleep is a program to determine the practical minimum sleep time
for a system, using the system-call clock_nanosleep()

The default mode is to run one million calls to clock_nanosleep() with
a requested sleep value of one nanosecond, measuring the actual time
elapsed for the calls. It's basically this (pseudo-code) loop:

``` C
	for (i=0, i < iterations; i++) {
		t1 = clock_gettime();
		clock_nanosleep(one_nanosecond);
		t2 = clock_gettime();
		duration = t2 - t1
		calc_min(duration);
		calc_max(duration);
		accum += duration;
	}
	mean = accum / iterations;
```

### ToDo
Might want to add a mode where the sleep time is increased until the
measured sleep time converges with the requested sleep time
