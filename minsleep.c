#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>
#include <getopt.h>

#define ITERATIONS 1000000
#define NSEC_PER_SEC 1000000000

/*
 * function to compare two timespecs and return the difference
 * in nanoseconds
 */
static inline int64_t calcdiff_ns(struct timespec t1, struct timespec t2)
{
	int64_t diff;
	diff = NSEC_PER_SEC * (int64_t)((int) t1.tv_sec - (int) t2.tv_sec);
	diff += ((int) t1.tv_nsec - (int) t2.tv_nsec);
	return diff;
}

/* function to convert a timespec structure to a value in nanoseconds */
static inline int64_t ts2int64(struct timespec ts)
{
	return (ts.tv_sec * NSEC_PER_SEC) + ts.tv_nsec;
}

static uint32_t iterations = ITERATIONS;
static uint32_t sleepval = 1;

static void process_args(char **argv, int argc)
{
	int c;
	struct option longopts[] = {
		{ "iterations",	required_argument,	0, 'i' },
		{ "sleep", 	required_argument,	0, 's' },
		{ 0,		0, 			0,  0  }
	};

	while (1) {
		c = getopt_long(argc, argv, "i:s:", longopts, NULL);
		if (c == -1)
			break;
		switch(c) {
		case 'i':
			iterations = atoi(optarg);
			break;
		case 's':
			sleepval = atoi(optarg);
			break;
		case '?':
			break;
		default:
			printf("?? getopt returned character code 0%o ??\n", c);
		}
	}
	printf("Loop iterations:  %d\n", iterations);
	printf("Sleep value:  %dns\n", sleepval);
}


/*
 * Loop 'iterations' times, calling clock_nanosleep() with a value of
 * the 'sleep' variable (represening some number of nanoseconds). Take a time stamp
 * before and after calling clock_nanosleep() and track the min and max values.
 * Accumulate the times and calculate a mean when the loop is finished.
 */
int main(int argc, char **argv)
{
	int i;
	int64_t min, max, mean, accum, interval;
	struct timespec t1, t2, sleeptime;

	process_args(argv, argc);

	min = INT_MAX;
	max = 0;
	accum = 0;
	for(i=0; i < iterations; i++) {
		sleeptime.tv_sec = 0;
		sleeptime.tv_nsec = sleepval;
		if (clock_gettime (CLOCK_MONOTONIC, &t1) < 0) {
			perror("first clock_gettime() failed");
			exit(-1);
		}
		clock_nanosleep(CLOCK_MONOTONIC, 0, &sleeptime, 0);
		if (clock_gettime(CLOCK_MONOTONIC, &t2) < 0) {
			perror("second clock_gettime() failed");
			exit(-1);
		}
		interval = calcdiff_ns(t2, t1);
		if (interval > max)
			max = interval;
		if (interval < min)
			min = interval;
		accum += interval;
	}
	printf("Min:  %5dns\n", min);
	printf("Max:  %5dns\n", max);
	printf("Mean: %5dns\n", accum / iterations);
}
