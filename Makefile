CFLAGS 	:=	-g -O2

minsleep:  minsleep.c
	$(CC) $(CFLAGS) -o minsleep minsleep.c


clean:
	rm -f *.o minsleep *~
